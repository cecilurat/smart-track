from django.db import models


class Company(models.Model):
	code = models.CharField("Codigo", max_length = 10, primary_key = True, blank = True)
	name = models.CharField("Nombre", max_length = 50)
	address = models.CharField("Direccion fisica", max_length = 50)
	phone = models.CharField("Telefono", max_length = 10)
	def save(self, *args, **kwargs):
		if self.code is None or self.code == "":
			try:
				item = Company.objects.all().latest("pk")
				new_pk = item.pk + 1
				self.code = "COMP" + str(f'{new_pk:06}')
			except:
				self.code = "COMP" + str(f'{1:06}')
		else:
			self.code = self.code
		self.name = self.name.upper()
		self.address = self.address.upper()
		super(Company, self).save(*args, **kwargs)

class Customer(models.Model):
	company = models.ForeignKey(Company, on_delete=models.CASCADE, related_name = "customers", null = True, blank = True)
	code = models.CharField("Codigo", max_length = 10, primary_key = True, blank = True)
	ruc = models.CharField("RUC", max_length = 20)
	address = models.CharField("Direccion fisica", max_length = 50)
	phone = models.CharField("Telefono", max_length = 20)
	name =  models.CharField("Nombre", max_length = 50)
	web = models.CharField("Web", max_length = 50)
	contact = models.CharField("Contacto", max_length = 50)
	phoneContact = models.CharField("Telefono del Contacto", max_length = 20, )
	mailContact = models.CharField("Correo de Contacto", max_length = 50)
	status = models.BooleanField("Estado", default = True)
	def save(self, *args, **kwargs):
		if self.code is None or self.code == "":
			try:
				item = Customer.objects.all().latest("pk")
				new_pk = item.pk + 1
				self.code = "CUST" + str(f'{new_pk:06}')
			except:
				self.code = "CUST" + str(f'{1:06}')
		else:
			self.code = self.code
		self.address = self.address.upper()
		self.phone = self.phone.upper()
		self.name = self.name.upper()
		self.contact = self.contact.upper()
		super(Customer, self).save(*args, **kwargs)

class Carrier(models.Model):
	company = models.ForeignKey(Company, on_delete=models.CASCADE, related_name = "carriers", null = True, blank = True)
	reason = models.CharField("Razon social", max_length = 50)
	code = models.CharField("Codigo", max_length = 20, primary_key = True, blank = True)
	ruc = models.CharField("RUC", max_length = 20)
	address = models.CharField("Direccion fisica", max_length = 50)
	phone = models.CharField("Telefono", max_length = 20)
	web = models.CharField("Web", max_length = 20)
	rep = models.CharField("Representante legal", max_length = 50)
	phoneRep = models.CharField("Telefono Representante legal", max_length = 20)
	mailRep = models.CharField("Correo Representante legal", max_length = 50)
	contact = models.CharField("Contacto", max_length = 20)
	phoneContact = models.CharField("Telefono de Contacto", max_length = 20)
	mailContact = models.CharField("Correo de Contacto", max_length = 50)
	status = models.BooleanField("Estado", default = True)
	def save(self, *args, **kwargs):
		if self.code is None or self.code == "":
			try:
				item = Carrier.objects.all().latest("pk")
				new_pk = item.pk + 1
				self.code = "CARR" + str(f'{new_pk:06}')
			except:
				self.code = "CARR" + str(f'{1:06}')
		else:
			self.code = self.code
		self.reason = self.reason.upper()
		self.ruc = self.ruc.upper()
		self.address = self.address.upper()
		self.phone = self.phone.upper()
		self.rep = self.rep.upper()
		self.phoneRep = self.phoneRep.upper()
		self.contact = self.contact.upper()
		self.phoneContact = self.phoneContact.upper()
		super(Carrier, self).save(*args, **kwargs)

class Driver(models.Model):
	DEPENDIENT = 1
	INDEPENDIENT = 2
	OPTIONS_TYPE = (
		(DEPENDIENT, 'DEPENDIENTE'),
		(INDEPENDIENT, 'INDEPENDIENTE'),
	)
	ONEGATIVO = 3
	OPOSITIVO = 4
	ANEGATIVO = 5
	APOSITIVO = 6
	BNEGATIVO = 7
	BPOSITIVO = 8
	ABNEGATIVO = 9
	ABPOSITIVO = 10
	OPTIONS_BLOOD = (
		(ONEGATIVO, 'O-'),
		(OPOSITIVO, 'O+'),
		(ANEGATIVO, 'A+'),
		(APOSITIVO, 'A-'),
		(BNEGATIVO, 'B-'),
		(BPOSITIVO, 'B+'),
		(ABNEGATIVO, 'AB-'),
		(ABPOSITIVO, 'AB+')
	)
	company = models.ForeignKey(Company, on_delete=models.CASCADE, related_name = "drivers", null = True, blank = True)
	carrier = models.ForeignKey(Carrier, on_delete=models.CASCADE, related_name = "drivers")
	license = models.CharField("Licencia", max_length = 20)
	code = models.CharField("Codigo", max_length = 10, primary_key = True, blank = True)
	firstName = models.CharField("Nombres", max_length = 50)
	lastName = models.CharField("Apellidos", max_length = 50)
	address = models.CharField("Direccion", max_length = 50)
	phone = models.CharField("Telefono", max_length = 20)
	birthdate = models.DateField("Fecha de nacimiento")
	doc = models.CharField("Documento de Identidad", max_length = 20)
	endLicense = models.DateField("Vencimiento de Licencia", null = True)
	type = models.IntegerField("Tipo de Trabajador", choices = OPTIONS_TYPE, default = DEPENDIENT)
	blood = models.IntegerField("Tipo de Sangre", choices = OPTIONS_BLOOD, default = ONEGATIVO)
	status = models.BooleanField("Estado", default = True)
	def save(self, *args, **kwargs):
		if self.code is None or self.code == "":
			try:
				item = Driver.objects.all().latest("pk")
				new_pk = item.pk + 1
				self.code = "DRIV" + str(f'{new_pk:06}')
			except:
				self.code = "DRIV" + str(f'{1:06}')
		else:
			self.code = self.code
		self.firstName = self.firstName.upper()
		self.lastName = self.lastName.upper()
		self.address = self.address.upper()
		self.phone = self.phone.upper()
		self.doc = self.doc.upper()
		super(Driver, self).save(*args, **kwargs)

class Vehicle(models.Model):
	company = models.ForeignKey(Company, on_delete=models.CASCADE, related_name = "vehicles", null = True, blank = True)
	carrier = models.ForeignKey(Carrier, on_delete=models.CASCADE, related_name = "vehicles")
	plateTract = models.CharField("Placa de Tracto", max_length = 10)
	brand = models.CharField("Marca", max_length = 20 )
	code = models.CharField("Codigo", max_length = 10, primary_key = True, blank = True)
	codeERP = models.CharField("Codigo ERP", max_length = 10, null = True, blank = True)
	weight = models.DecimalField("Peso", max_digits = 7, decimal_places = 2)
	volume = models.DecimalField("Volumen", max_digits = 7, decimal_places = 2)
	width = models.DecimalField("Ancho", max_digits = 7, decimal_places = 2)
	long = models.DecimalField("Largo", max_digits = 7, decimal_places = 2)
	height = models.DecimalField("Altura", max_digits = 7, decimal_places = 2)
	capacity = models.DecimalField("Capacidad", max_digits = 7, decimal_places = 2)
	kinds = models.CharField("Clases", max_length = 200)
	category = models.CharField("Categoria", max_length = 200)
	status = models.BooleanField("Estado", default = True)
	def save(self, *args, **kwargs):
		if self.code is None or self.code == "":
			try:
				item = Vehicle.objects.all().latest("pk")
				new_pk = item.pk + 1
				self.code = "VEHI" + str(f'{new_pk:06}')
			except:
				self.code = "VEHI" + str(f'{1:06}')
		else:
			self.code = self.code
		self.plateTract = self.plateTract.upper()
		self.brand = self.brand.upper()
		self.codeERP = self.codeERP.upper()
		super(Vehicle, self).save(*args, **kwargs)

class Tumbril(models.Model):
	company = models.ForeignKey(Company, on_delete=models.CASCADE, related_name = "tumbrils", null = True, blank = True)
	carrier = models.ForeignKey(Carrier, on_delete=models.CASCADE, related_name = "tumbrils")
	plateTumbril = models.CharField("Placa de Carreta", max_length = 10)
	codeMTC = models.CharField("Codigo MTC", max_length = 10)
	code = models.CharField("Codigo", max_length = 10, primary_key = True, blank = True)
	weight = models.DecimalField("Peso", max_digits = 7, decimal_places = 2)
	volume = models.DecimalField("Volumen", max_digits = 7, decimal_places = 2)
	width = models.DecimalField("Ancho", max_digits = 7, decimal_places = 2)
	long = models.DecimalField("Largo", max_digits = 7, decimal_places = 2)
	height = models.DecimalField("Altura", max_digits = 7, decimal_places = 2)
	status = models.BooleanField("Estado", default = True)
	def save(self, *args, **kwargs):
		if self.code is None or self.code == "":
			try:
				item = Tumbril.objects.all().latest("pk")
				new_pk = item.pk + 1
				self.code = "TUMB" + str(f'{new_pk:06}')
			except:
				self.code = "TUMB" + str(f'{1:06}')
		else:
			self.code = self.code
		self.plateTumbril = self.plateTumbril.upper()
		self.codeMTC = self.codeMTC.upper()
		super(Tumbril, self).save(*args, **kwargs)		

class CenterType(models.Model):
	company = models.ForeignKey(Company, on_delete=models.CASCADE, related_name = "centers_types", null = True, blank = True)
	name = models.CharField("Nombre", max_length = 50)
	code = models.CharField("Codigo", max_length = 10, primary_key = True)
	description = models.CharField("Descripcion", max_length = 100)
	status = models.BooleanField("Estado", default = True)
	def save(self, *args, **kwargs):
		if self.code is None or self.code == "":
			try:
				item = CenterType.objects.all().latest("pk")
				new_pk = item.pk + 1
				self.code = "TYPE" + str(f'{new_pk:06}')
			except:
				self.code = "TYPE" + str(f'{1:06}')
		else:
			self.code = self.code
		self.name = self.name.upper()
		self.description = self.description.upper()
		super(CenterType, self).save(*args, **kwargs)

class CenterOwner(models.Model):
	company = models.ForeignKey(Company, on_delete=models.CASCADE, related_name = "centers_owners", null = True, blank = True)
	code = models.CharField("Codigo", max_length = 10, primary_key = True)
	reason = models.CharField("Razon social", max_length = 50)
	ruc = models.CharField("RUC", max_length = 10)
	contact = models.CharField("Contacto", max_length = 20)
	phoneContact = models.CharField("Telefono", max_length = 20)
	mailContact = models.CharField("Correo", max_length = 20)
	nameCompany = models.CharField("Nombre de la compañia", max_length = 20)
	status = models.BooleanField("Estado", default = True)
	def save(self, *args, **kwargs):
		if self.code is None or self.code == "":
			try:
				item = CenterOwner.objects.all().latest("pk")
				new_pk = item.pk + 1
				self.code = "OWNE" + str(f'{new_pk:06}')
			except:
				self.code = "OWNE" + str(f'{1:06}')
		else:
			self.code = self.code
		self.reason = self.reason.upper()
		self.ruc = self.ruc.upper()
		self.contact = self.contact.upper()
		self.nameCompany = self.nameCompany.upper()
		super(CenterOwner, self).save(*args, **kwargs)		

class Center(models.Model):
	company = models.ForeignKey(Company, on_delete=models.CASCADE, related_name = "centers", null = True, blank = True)
	location = models.CharField("Localidad", max_length = 20)
	code = models.CharField("Codigo", max_length = 10, primary_key = True)
	type = models.ForeignKey(CenterType, on_delete=models.CASCADE)
	address = models.CharField("Direccion", max_length = 50)
	latitude = models.CharField("Latitud", max_length = 10)
	longitude = models.CharField("Longitud", max_length = 10)
	timeini = models.DateTimeField("Hora de inicio de atención", null = True)
	timefin = models.DateTimeField("Hora de fin de atención", null = True)
	customer = models.ForeignKey(Customer, on_delete=models.CASCADE, related_name = "centers")
	owner = models.ForeignKey(CenterOwner, on_delete=models.CASCADE, related_name = "centers")
	status = models.BooleanField("Estado", default = True)
	def save(self, *args, **kwargs):
		if self.code is None or self.code == "":
			try:
				item = Center.objects.all().latest("pk")
				new_pk = item.pk + 1
				self.code = "CENT" + str(f'{new_pk:06}')
			except:
				self.code = "CENT" + str(f'{1:06}')
		else:
			self.code = self.code
		self.location = self.location.upper()
		self.address = self.address.upper()
		super(Center, self).save(*args, **kwargs)			

class Warehouse(models.Model):
	company = models.ForeignKey(Company, on_delete=models.CASCADE, related_name = "warehouses", null = True, blank = True)
	center = models.ForeignKey(Center, on_delete = models.CASCADE, related_name = "warehouses")
	code = models.CharField("Codigo", max_length = 10, primary_key = True)
	name = models.CharField("Nombre", max_length = 50)
	address = models.CharField("Direccion", max_length = 50)
	latitude = models.CharField("Latitud", max_length = 10)
	longitude = models.CharField("Longitud", max_length = 10)
	timeini = models.DateTimeField("Hora de inicio de atención", null = True)
	timefin = models.DateTimeField("Hora de fin de atención", null = True)
	status = models.BooleanField("Estado", default = True)
	def save(self, *args, **kwargs):
		if self.code is None or self.code == "":
			try:
				item = Warehouse.objects.all().latest("pk")
				new_pk = item.pk + 1
				self.code = "WARE" + str(f'{new_pk:06}')
			except:
				self.code = "WARE" + str(f'{1:06}')
		else:
			self.code = self.code
		self.name = self.name.upper()
		self.address = self.address.upper()
		super(Warehouse, self).save(*args, **kwargs)

class Door(models.Model):
	company = models.ForeignKey(Company, on_delete=models.CASCADE, related_name = "doors", null = True, blank = True)
	center = models.ForeignKey(Center, on_delete=models.CASCADE, related_name = "doors")
	warehouse = models.ForeignKey(Warehouse, on_delete=models.CASCADE, related_name= "doors")
	code = models.CharField("Codigo", max_length = 10, primary_key = True)
	name = models.CharField("Nombre", max_length = 50)
	address = models.CharField("Direccion", max_length = 50)
	latitude = models.CharField("Latitud", max_length = 10)
	longitude = models.CharField("Longitud", max_length = 10)
	timeini = models.DateTimeField("Hora de inicio de atención", null = True)
	timefin = models.DateTimeField("Hora de fin de atención", null = True)
	status = models.BooleanField("Estado", default = True)
	def save(self, *args, **kwargs):
		if self.code is None or self.code == "":
			try:
				item = Door.objects.all().latest("pk")
				new_pk = item.pk + 1
				self.code = "DOOR" + str(f'{new_pk:06}')
			except:
				self.code = "DOOR" + str(f'{1:06}')
		else:
			self.code = self.code
		self.name = self.name.upper()
		self.address = self.address.upper()
		super(Door, self).save(*args, **kwargs)	

class UnitMeasure(models.Model):
	company = models.ForeignKey(Company, on_delete=models.CASCADE, related_name = "units_measures", null = True, blank = True)
	code = models.CharField("Codigo", max_length = 10, primary_key = True)
	name = models.CharField("Nombre", max_length = 50)
	weight = models.DecimalField("Peso", max_digits = 7, decimal_places = 2)
	volume = models.DecimalField("Volumen", max_digits = 7, decimal_places = 2)
	width = models.DecimalField("Ancho", max_digits = 7, decimal_places = 2)
	long = models.DecimalField("Largo", max_digits = 7, decimal_places = 2)
	height = models.DecimalField("Altura", max_digits = 7, decimal_places = 2)
	status = models.BooleanField("Estado", default = True)
	def save(self, *args, **kwargs):
		if self.code is None or self.code == "":
			try:
				item = UnitMeasure.objects.all().latest("pk")
				new_pk = item.pk + 1
				self.code = "UNIT" + str(f'{new_pk:06}')
			except:
				self.code = "UNIT" + str(f'{1:06}')
		else:
			self.code = self.code
		self.name = self.name.upper()
		super(UnitMeasure, self).save(*args, **kwargs)

class Route(models.Model):
	company = models.ForeignKey(Company, on_delete=models.CASCADE, related_name = "routes", null = True, blank = True)
	code = models.CharField("Codigo", max_length = 10, primary_key = True)
	pointStart = models.ForeignKey(Center, on_delete=models.CASCADE, related_name = "routes_starts")
	pointEnd = models.ForeignKey(Center, on_delete=models.CASCADE, related_name = "routes_ends")
	distance = models.DecimalField("Distancia", max_digits = 7, decimal_places = 2)
	time = models.DecimalField("Tiempo", max_digits = 7, decimal_places = 2)
	cost = models.DecimalField("Costo", max_digits = 7, decimal_places = 2)
	price = models.DecimalField("Precio", max_digits = 7, decimal_places = 2)
	status = models.BooleanField("Estado", default = True)
	def save(self, *args, **kwargs):
		if self.code is None or self.code == "":
			try:
				item = Route.objects.all().latest("pk")
				new_pk = item.pk + 1
				self.code = "ROUT" + str(f'{new_pk:06}')
			except:
				self.code = "ROUT" + str(f'{1:06}')
		else:
			self.code = self.code
		super(Route, self).save(*args, **kwargs)

class ServiceType(models.Model):
	company = models.ForeignKey(Company, on_delete=models.CASCADE, related_name = "services_types", null = True, blank = True)
	code = models.CharField("Codigo", max_length = 10, primary_key = True)
	codeERP = models.CharField("Codigo ERP", max_length = 10)
	name = models.CharField("Nombre", max_length = 50)
	description = models.CharField("Descripción", max_length = 100)
	status = models.BooleanField("Estado", default = True)
	def save(self, *args, **kwargs):
		if self.code is None or self.code == "":
			try:
				item = ServiceType.objects.all().latest("pk")
				new_pk = item.pk + 1
				self.code = "SERV" + str(f'{new_pk:06}')
			except:
				self.code = "SERV" + str(f'{1:06}')
		else:
			self.code = self.code
		self.codeERP = self.codeERP.upper()
		self.name = self.name.upper()
		self.description = self.description.upper()
		super(ServiceType, self).save(*args, **kwargs)

class Event(models.Model):
	PICKUP = 1
	TRANSIT = 2
	DELIVERY = 3
	PHOTOS = 4
	SIGNATURE = 5
	OPTIONS_TYPE = (
		(PICKUP, 'RECOJO'),
		(TRANSIT, 'TRANSITO'),
		(DELIVERY, 'ENTREGA'),
		(PHOTOS, 'FOTOS'),
		(SIGNATURE, 'FIRMA DIGITAL'),
	)
	company = models.ForeignKey(Company, on_delete=models.CASCADE, related_name = "events", null = True, blank = True)
	code = models.CharField("Codigo", max_length = 10, primary_key = True)
	type = models.IntegerField("Tipo de Evento", choices = OPTIONS_TYPE, default = PICKUP)
	name = models.CharField("Nombre", max_length = 50)
	status = models.BooleanField("Estado", default = True)
	def save(self, *args, **kwargs):
		if self.code is None or self.code == "":
			try:
				item = Event.objects.all().latest("pk")
				new_pk = item.pk + 1
				self.code = "EVEN" + str(f'{new_pk:06}')
			except:
				self.code = "EVEN" + str(f'{1:06}')
		else:
			self.code = self.code
		self.name = self.name.upper()
		super(Event, self).save(*args, **kwargs)
	
class AdditionalCost(models.Model):
	company = models.ForeignKey(Company, on_delete=models.CASCADE, related_name = "costs", null = True, blank = True)
	code = models.CharField("Codigo", max_length = 10, primary_key = True)
	name = models.CharField("Nombre", max_length = 50)
	valueMin = models.DecimalField("Valor minimo", max_digits = 7, decimal_places = 2)
	valueMax = models.DecimalField("Valor maximo", max_digits = 7, decimal_places = 2)
	docObligatory = models.BooleanField("Documento obligatorio", default = True)
	status = models.BooleanField("Estado", default = True)
	def save(self, *args, **kwargs):
		if self.code is None or self.code == "":
			try:
				item = AdditionalCost.objects.all().latest("pk")
				new_pk = item.pk + 1
				self.code = "COST" + str(f'{new_pk:06}')
			except:
				self.code = "COST" + str(f'{1:06}')
		else:
			self.code = self.code
		self.name = self.name.upper()
		super(AdditionalCost, self).save(*args, **kwargs)

class GroupTractTumbril(models.Model):
	company = models.ForeignKey(Company, on_delete=models.CASCADE, related_name="groups", null = True, blank = True)
	code = models.CharField("Codigo", max_length = 10, primary_key = True)
	status = models.BooleanField("Estado", default = True)
	def save(self, *args, **kwargs):
		if self.code is None or self.code == "":
			try:
				item = GroupTractTumbril.objects.all().latest("pk")
				new_pk = item.pk + 1
				self.code = "GROU" + str(f'{new_pk:06}')
			except:
				self.code = "GROU" + str(f'{1:06}')
		else:
			self.code = self.code
		super(GroupTractTumbril, self).save(*args, **kwargs)