from django.shortcuts import render
from django.shortcuts import get_object_or_404
from .serializers import *
from rest_framework import viewsets
from rest_framework.response import Response


# Create your views here.

class CompanyViewSet(viewsets.ModelViewSet):
	serializer_class = CompanySerializer
	queryset = Company.objects.all()

class CustomerViewSet(viewsets.ModelViewSet):
	serializer_class = CustomerSerializer
	queryset = Customer.objects.all()

class CarrierViewSet(viewsets.ModelViewSet):
	serializer_class = CarrierSerializer
	queryset = Carrier.objects.all()

class DriverViewSet(viewsets.ModelViewSet):
	serializer_class = DriverSerializer
	queryset = Driver.objects.all()

class VehicleViewSet(viewsets.ModelViewSet):
	serializer_class = VehicleSerializer
	queryset = Vehicle.objects.all()

class TumbrilViewSet(viewsets.ModelViewSet):
	serializer_class = TumbrilSerializer
	queryset = Tumbril.objects.all()

class CenterTypeViewSet(viewsets.ModelViewSet):
	serializer_class = CenterTypeSerializer
	queryset = CenterType.objects.all()

class CenterOwnerViewSet(viewsets.ModelViewSet):
	serializer_class = CenterOwnerSerializer
	queryset = CenterOwner.objects.all()

class CenterViewSet(viewsets.ModelViewSet):
	serializer_class = CenterSerializer 
	queryset = Center.objects.all()

class WarehouseViewSet(viewsets.ModelViewSet):
	serializer_class = WarehouseSerializer
	queryset = Warehouse.objects.all()

class DoorViewSet(viewsets.ModelViewSet):
	serializer_class = DoorSerializer
	queryset = Door.objects.all()

class UnitMeasureViewSet(viewsets.ModelViewSet):
	serializer_class = UnitMeasureSerializer
	queryset = Door.objects.all()

class RouteViewSet(viewsets.ModelViewSet):
	serializer_class = RouteSerializer
	queryset = Route.objects.all()

class ServiceTypeViewSet(viewsets.ModelViewSet):
	serializer_class = ServiceTypeSerializer
	queryset = ServiceType.objects.all()

class EventViewSet(viewsets.ModelViewSet):
	serializer_class = EventSerializer
	queryset = Event.objects.all()

class AdditionalCostViewSet(viewsets.ModelViewSet):
	serializer_class = AdditionalCostSerializer
	queryset = AdditionalCost.objects.all()

	