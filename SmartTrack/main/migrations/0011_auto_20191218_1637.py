# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2019-12-18 16:37
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0010_auto_20191218_1634'),
    ]

    operations = [
        migrations.AlterField(
            model_name='vehicle',
            name='codeERP',
            field=models.CharField(blank=True, max_length=10, null=True, verbose_name='Codigo ERP'),
        ),
    ]
