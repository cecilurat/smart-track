# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2019-12-18 15:09
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0002_grouptracttumbril'),
    ]

    operations = [
        migrations.AlterField(
            model_name='additionalcost',
            name='company',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='costs', to='main.Company'),
        ),
        migrations.AlterField(
            model_name='carrier',
            name='company',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='carriers', to='main.Company'),
        ),
        migrations.AlterField(
            model_name='center',
            name='company',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='centers', to='main.Company'),
        ),
        migrations.AlterField(
            model_name='centerowner',
            name='company',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='centers_owners', to='main.Company'),
        ),
        migrations.AlterField(
            model_name='centertype',
            name='company',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='centers_types', to='main.Company'),
        ),
        migrations.AlterField(
            model_name='company',
            name='code',
            field=models.CharField(blank=True, max_length=10, primary_key=True, serialize=False, verbose_name='Codigo'),
        ),
        migrations.AlterField(
            model_name='customer',
            name='company',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='customers', to='main.Company'),
        ),
        migrations.AlterField(
            model_name='door',
            name='company',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='doors', to='main.Company'),
        ),
        migrations.AlterField(
            model_name='driver',
            name='company',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='drivers', to='main.Company'),
        ),
        migrations.AlterField(
            model_name='event',
            name='company',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='events', to='main.Company'),
        ),
        migrations.AlterField(
            model_name='grouptracttumbril',
            name='company',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='groups', to='main.Company'),
        ),
        migrations.AlterField(
            model_name='route',
            name='company',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='routes', to='main.Company'),
        ),
        migrations.AlterField(
            model_name='servicetype',
            name='company',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='services_types', to='main.Company'),
        ),
        migrations.AlterField(
            model_name='tumbril',
            name='company',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='tumbrils', to='main.Company'),
        ),
        migrations.AlterField(
            model_name='unitmeasure',
            name='company',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='units_measures', to='main.Company'),
        ),
        migrations.AlterField(
            model_name='vehicle',
            name='company',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='vehicles', to='main.Company'),
        ),
        migrations.AlterField(
            model_name='warehouse',
            name='company',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='warehouses', to='main.Company'),
        ),
    ]
