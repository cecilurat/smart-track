from rest_framework import serializers
from .models import *

class CompanySerializer(serializers.ModelSerializer):
	class Meta:
		model = Company
		fields = '__all__'

class CustomerSerializer(serializers.ModelSerializer):
	class Meta:
		model = Customer
		fields = '__all__'

class CarrierSerializer(serializers.ModelSerializer):
	class Meta:
		model = Carrier
		fields = '__all__'

class DriverSerializer(serializers.ModelSerializer):
	class Meta:
		model = Driver
		fields = '__all__'

class VehicleSerializer(serializers.ModelSerializer):
	class Meta:
		model = Vehicle
		fields = '__all__'

class TumbrilSerializer(serializers.ModelSerializer):
	class Meta:
		model = Tumbril
		fields = '__all__'

class CenterTypeSerializer(serializers.ModelSerializer):
	class Meta:
		model = CenterType
		fields = '__all__'

class CenterOwnerSerializer(serializers.ModelSerializer):
	class Meta:
		model = CenterOwner
		fields = '__all__'

class CenterSerializer(serializers.ModelSerializer):
	class Meta:
		model = Center
		fields = '__all__'

class WarehouseSerializer(serializers.ModelSerializer):
	class Meta:
		model = Warehouse
		fields = '__all__'

class DoorSerializer(serializers.ModelSerializer):
	class Meta:
		model = Door
		fields = '__all__'

class UnitMeasureSerializer(serializers.ModelSerializer):
	class Meta:
		model = UnitMeasure
		fields = '__all__'

class RouteSerializer(serializers.ModelSerializer):
	class Meta:
		model = Route
		fields = '__all__'

class ServiceTypeSerializer(serializers.ModelSerializer):
	class Meta:
		model = ServiceType
		fields = '__all__'

class EventSerializer(serializers.ModelSerializer):
	class Meta:
		model = Event
		fields = '__all__'

class AdditionalCostSerializer(serializers.ModelSerializer):
	class Meta:
		model = AdditionalCost
		fields = '__all__'