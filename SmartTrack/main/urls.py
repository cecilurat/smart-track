
from django.conf.urls import url
from .views import *

from rest_framework import routers

router = routers.SimpleRouter()
router.register(r'company', CompanyViewSet)
router.register(r'customer', CustomerViewSet)
router.register(r'carrier', CarrierViewSet)
router.register(r'driver', DriverViewSet)
router.register(r'vehicle', VehicleViewSet)
router.register(r'tumbril', TumbrilViewSet)
router.register(r'center_type', CenterTypeViewSet)
router.register(r'center_owner', CenterOwnerViewSet)
router.register(r'center', CenterViewSet)
router.register(r'warehouse', WarehouseViewSet)
router.register(r'door', DoorViewSet)
router.register(r'unit_measure', UnitMeasureViewSet)
router.register(r'route', RouteViewSet)
router.register(r'service_type', ServiceTypeViewSet)
router.register(r'event', EventViewSet)
router.register(r'additional_cost', AdditionalCostViewSet)


#router.register(r'report', ReportViewSet, basename="reports")


urlpatterns = router.urls


urlpatterns += [
	
]