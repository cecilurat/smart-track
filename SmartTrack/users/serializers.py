from rest_framework import serializers
from .models import *

class UserTypeSerializer(serializers.ModelSerializer):
	class Meta:
		model = UserType
		fields = '__all__'

class SysUserSerializer(serializers.ModelSerializer):
	class Meta:
		model = SysUser
		fields = '__all__'

class SysUserAppSerializer(serializers.ModelSerializer):
	class Meta:
		model = SysUserApp
		fields = '__all__'

