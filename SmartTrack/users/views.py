from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from django.contrib.auth import authenticate, get_user_model
from rest_framework.permissions import IsAuthenticated
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.views import ObtainAuthToken
from django.shortcuts import render
from django.shortcuts import get_object_or_404
from .serializers import *
from rest_framework import viewsets
from rest_framework.response import Response

User = get_user_model()

# Create your views here.

class UserTypeViewSet(viewsets.ModelViewSet):
	serializer_class = UserTypeSerializer
	queryset = UserType.objects.all()

class SysUserViewSet(viewsets.ModelViewSet):
	serializer_class = SysUserSerializer
	queryset = SysUser.objects.all()

class SysUserAppViewSet(viewsets.ModelViewSet):
	serializer_class = SysUserApp
	queryset = SysUserApp.objects.all()

class UserAuthToken(ObtainAuthToken):
	def post(self, request, *args, **kwargs):
		serializer = self.serializer_class(data=request.data, context={'request': request})
		serializer.is_valid(raise_exception=True)
		user = serializer.validated_data['user']
		token, created = Token.objects.get_or_create(user=user)
		return Response({'user': user.first_name,'token': token.key})

class Logout(APIView):
    def get(self, request, format=None):
        request.user.auth_token.delete()
       	content = {'response': 'success'}
       	return Response(content)
