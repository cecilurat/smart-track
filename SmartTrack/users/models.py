from django.db import models
from main.models import Company, Driver, GroupTractTumbril
from django.contrib.auth.models import AbstractUser

class UserType(models.Model):
	company = models.ForeignKey(Company, on_delete=models.CASCADE, related_name = "user_types")
	code = models.CharField("Codigo", max_length = 10, primary_key = True)
	name = models.CharField("Nombre", max_length = 50)
	status = models.BooleanField("Estado", default = True)
	def save(self, *args, **kwargs):
		if self.code is None or self.code == "":
			try:
				item = UserType.objects.all().latest("pk")
				new_pk = item.pk + 1
				self.code = "TUSE" + str(f'{new_pk:06}')
			except:
				self.code = "TUSE" + str(f'{1:06}')
		else:
			self.code = self.code
		self.name = self.name.upper()
		super(UserType, self).save(*args, **kwargs)

class SysUser(AbstractUser):
	code = models.CharField("Codigo", max_length = 10, primary_key = True)
	company = models.ForeignKey(Company, on_delete=models.CASCADE, related_name = "users")
	typeUser = models.ForeignKey(UserType, on_delete=models.CASCADE, related_name = "users")
	status = models.BooleanField("Estado", default = True)
	def save(self, *args, **kwargs):
		if self.code is None or self.code == "":
			try:
				item = SysUser.objects.all().latest("pk")
				new_pk = item.pk + 1
				self.code = "USER" + str(f'{new_pk:06}')
			except:
				self.code = "USER" + str(f'{1:06}')
		else:
			self.code = self.code
		super(SysUser, self).save(*args, **kwargs)


class SysUserApp(SysUser):
	driver = models.ForeignKey(Driver, on_delete=models.CASCADE, related_name = "drivers")
	plate = models.ForeignKey(GroupTractTumbril, on_delete=models.CASCADE, related_name = "drivers")
	def save(self, *args, **kwargs):
		if self.code is None or self.code == "":
			try:
				item = SysUserApp.objects.all().latest("pk")
				new_pk = item.pk + 1
				self.code = "SAPP" + str(f'{new_pk:06}')
			except:
				self.code = "SAPP" + str(f'{1:06}')
		else:
			self.code = self.code
		super(SysUserApp, self).save(*args, **kwargs)
