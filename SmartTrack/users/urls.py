
from django.conf.urls import url
from .views import *

from rest_framework import routers

router = routers.SimpleRouter()
router.register(r'user_type', UserTypeViewSet)
router.register(r'sys_user', SysUserViewSet)
router.register(r'sys_user_app', SysUserAppViewSet)



#router.register(r'report', ReportViewSet, basename="reports")


urlpatterns = router.urls


urlpatterns += [
	
	url(r'^api-token-auth/$', UserAuthToken.as_view(), name="login"),
	url(r'^logout/$', Logout.as_view(), name="logout"),

]