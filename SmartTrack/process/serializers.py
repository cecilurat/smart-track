from rest_framework import serializers
from .models import *

class OrderHeadSerializer(serializers.ModelSerializer):
	class Meta:
		model = OrderHead
		fields = '__all__'

class OrderDetailSerializer(serializers.ModelSerializer):
	class Meta:
		model = OrderDetail
		fields = '__all__'
		
