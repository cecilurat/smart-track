from django.shortcuts import render
from .serializers import *
from .views import *
from rest_framework.views import APIView
from rest_framework import viewsets
from rest_framework.response import Response


class OrderViewSet(viewsets.ViewSet):
	def create(self, request):
		order_serializer = OrderHeadSerializer(data = request.data.get("head"))
		if order_serializer.is_valid():
			order = order_serializer.save()
			order.company = request.user.company
			order.save()
		else:
			return Response(order_serializer.errors())
		for detail in request.data.get("details"):
			detail_serializer = OrderDetailSerializer(data = detail)
			if detail_serializer.is_valid():
				detail = detail_serializer.save()
				detail.order = order
				detail.save()
			else:
				return Response(detail_serializer.errors())
		return Response({"response":"success"})
	