from django.db import models
from main.models import Company, Customer, Center, ServiceType


class OrderHead(models.Model):
	company = models.ForeignKey(Company, on_delete=models.CASCADE, related_name = "orders", null = True)
	code = models.CharField("Codigo", max_length = 10, primary_key = True)
	customer = models.ForeignKey(Customer, on_delete=models.CASCADE, related_name = "orders")
	weight = models.DecimalField("Peso", max_digits = 7, decimal_places = 2)
	volume = models.DecimalField("Volumen", max_digits = 7, decimal_places = 2)
	service = models.ForeignKey(ServiceType, on_delete=models.CASCADE, related_name = "orders")


class OrderDetail(models.Model):
	order = models.ForeignKey(OrderHead, on_delete=models.CASCADE, related_name = "details", null = True)
	centerOrigin = models.ForeignKey(Center, on_delete=models.CASCADE, related_name = "orders_origin")
	centerDestiny = models.ForeignKey(Center, on_delete=models.CASCADE, related_name = "orders_destiny")
	date = models.DateTimeField("Fecha y hora", null = True)
	distance = models.DecimalField("Distancia", max_digits = 7, decimal_places = 2)
	time = models.DecimalField("Tiempo", max_digits = 7, decimal_places = 2)
	weight = models.DecimalField("Peso", max_digits = 7, decimal_places = 2)
	volume = models.DecimalField("Volumen", max_digits = 7, decimal_places = 2)
	observation = models.TextField("Observacion", null = True, blank = True)
