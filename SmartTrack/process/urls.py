
from django.conf.urls import url
from .views import *

from rest_framework import routers

router = routers.SimpleRouter()
#router.register(r'customer', CustomerViewSet)

router.register(r'order', OrderViewSet, basename="orders")


urlpatterns = router.urls


urlpatterns += [
	
]